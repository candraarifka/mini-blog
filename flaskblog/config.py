import os
import pymysql


class Config:
    SECRET_KEY = 'rahasia-rahasia'
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:123456@localhost/flaskblog'
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = os.environ.get('EMAIL_USER')
    MAIL_PASSWORD = os.environ.get('EMAIL_PASS')
