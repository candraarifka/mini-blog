import re
import operator

mystr = 'This, is is a string, with words!'
wordList = re.sub("[^\w]", " ",  mystr).split()
d={}

for x in wordList:
    d[x] = d.get(x,0)+1


print(d)

dictlist=[]

for key, value in d.items():
    temp = [key,value]
    dictlist.append(temp)
print(dictlist)

a = max(d.items(), key=operator.itemgetter(1))[0]
print(a)
